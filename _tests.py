import sys

from hypothesis import given, example
from hypothesis.strategies import *

import layer

#2560x1600 is the biggest screen size commercially available as of 2016.
@given( width = integers(0, 2560), height = integers(0, 1600) )
def test_instantiating_layer(width, height):
    mylayer=layer.Layer((width, height))

if __name__ == '__main__':
    import pytest
    sys.exit(pytest.main("_tests.py"))