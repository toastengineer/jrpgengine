"""
Contains user interface primitives; textboxes, menu boxes... lotta boxes, actually.

This stuff is all designed to get rendered on to a Layer, which you'll then mix and blit to the screen.
"""
import pygame

import layer

class Window:
    """Simply a box meant to contain other elements inside it.

    You can give it a Layer at instantiation-time or set its .originlayer attribute; either way will do the same thing.
    It'll grab a sublayer of that layer and set it as its .layer attribute, then register itself as a renderer to that
    layer (the Window itself IS the renderer object.) Changing .originlayer later on is okay; the Window will make a new
    ._layer, unregister itself from the old one, and register itself with the new one. Changing ._layer, on the other
    hand, is a very bad idea. ._layer will be None under certain circumstances, by the way, since the Window invalidates
    it when its moved; basically, just don't mess with it.

    Windows, like all UI elements, use a fractional coordinate system; its x and y attributes are float values from 0
    to 1; 0 represents the extreme left or top of the layer, 1 represents the extreme right or bottom. These coordinates
    represent the position of the top left corner of the window, but the window also has .centerx and .centery attributes
    that work the same way.

    The width and height attributes are similar; they're fractions of the total width and height of the layer.

    So to create a Window spanning the entire surface of its Layer, instantiate it with Window(0, 0, 1, 1, mylayer)
    To create a Window that has half its layer's width and height and is centered in the exact middle of its layer,
    instantiate it like this:
    mywindow=Window(0, 0, 0.5, 0.5, mylayer)
    mywindow.center = (0.5, 0.5)

    This first creates it in the upper left corner of the layer and then moves it to the center. You can't determine the
    center of a rectangle until its size is known, so you have to instantiate it and then move it with .center.

    Or, to create a Window taking up the top left corner of the layer:
    mywindow.Window(0, 0, 1/4, 1/4)
    mywindow.center=(1/4, 1/4)

    And if you want to move that Window to the bottom right corner:
    mywindow.center=(3/4, 3/4)

    A Window has the following position-related attributes:
    x : The distance from the left edge of its .originlayer to the left edge of the Window, from 0 to 1
    y : The distance from the top edge of its .originlayer to the top edge of the Window, from 0 to 1
    pos : a tuple of x and y, in that order.

    leftdist : Another name for x; sometimes its clearer what you're doing when refer to one over the other.
    topdist : Another name for y.
    topleft : Another name for pos.

    centerx : The distance from the left edge of its .originlayer to the middle of the window, from 0 to 1
    centery : The distance from the top edge of its .originlayer to the middle of the window, from 0 to 1
    centerpos : A tuple of centerx and centery, in that order.

    width : The fraction of the width of the .originlayer that the window takes up, from 0 to 1.
    height : The fraction of the height of the .originlayer that the window takes up, from 0 to 1.
    size : A tuple of (width, height).

    rightdist : What fraction the length of the line from the left side of the window to the right side is of the length
    of the line from the left side of the window to the right side of its .originlayer, from 0 to 1.
    In other words, this is the fraction of the space from the left of the window to the right of the screen that the
    window takes up.
    bottomdist : What fraction the length of the line from the top of the window to the bottom is of the length of the
    line from the top of the window to the bottom of its .originlayer, from 0 to 1.
    bottomright :

    All the above attrs can be set as well as read and will be kept internally consistent.

    Adjusting the position of the window with pos or centerpos or their components keeps the width and height of the
    window constant. Similarly, adjusting width, height, rightdist and bottomdist leaves x and y unchanged.

    A consequence of this is that changing the width and height _will_ change the .centerpos of the window; set the
    .centerpos to its old value after changing the size if you want to preserve it.
    """

    def __init__(self, x, y, width=None, height=None, bottomdist=None, rightdist=None, originlayer=None):
        """x and y are fractional position values, floats from 0 to 1, that represent how far along the .originlayer's
        width and height respectively. Width and height are fractional values that represent. Originlayer is the layer
        that this window will render its contents to; you can leave this as None, but the Window will be useless
        until it's given an .originlayer."""
        self.x, self.y = x, y

        if width is not None and height is not None:
            self.width, self.height = width, height
        elif bottomdist is not None and rightdist is not None:
            self.bottomdist, self.rightdist = bottomdist, rightdist
        else:
            raise ValueError("Width & height/bottomdist & rightdist were not provided or are invalid.")

        self.originlayer=originlayer #type: layer.Layer
        self._layer=None #type: layer.Layer

    def absolute(self, theattr):
        """Get absolute pixel values, coordinates on the .originlayer's surface, corresponding to the fractional
        position attribute on the Window.

        I.e. get_absolute_coords("topleft") gives you a tuple of x, y coordinates representing the line from the top
        left corner of the layer to the top left corner of the window. get_absolute_coords("width") will give the
        actual width of the window, as represented on the Layer's Surface and on the screen (barring Windows dpi
        fuckery.)

        All the fractional positional attributes specified in the Window's docstring work with this."""

        therect = self.originlayer.backing.get_rect() #type: pygame.Rect
        {
            "x" : lambda: therect.width * self.x,
            "leftdist": lambda: therect.width*self.x,
            "y": lambda: therect.height*self.y,
            "topdist": lambda: therect.height*self.y,
            "pos": lambda: (self.absolute('x'), self.absolute('y')),
            "topleft": lambda: (self.absolute('x'),self.absolute('y')),
            "centerx": lambda: self.centerx * therect.width,
            "centery": lambda: self.centery * therect.height,
            "width": lambda: self.width * therect.width,
            "height": lambda: self.height * therect.height,
            "size": (self.absolute('width'), self.absolute("height"))

        }


    @property
    def originlayer(self):
        return self._originlayer
    @originlayer.setter
    def originlayer(self, neworiginlayer):
        #If we're moving to a new layer, we need to remove ourself from the old one's set of things to render.
        self._originlayer.renderers.remove(self)

        #Then put outself in the new one's.
        self._originlayer = neworiginlayer #type: layer.Layer
        self._originlayer.renderers.append(self)

        #and update our sublayer
        self._sublayer=None



if __name__ == '__main__':
    pass