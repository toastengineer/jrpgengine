"""
The Layer is the most basic rendering primitive. Everything else puts rendering callables into the Layer, which it will
then process and blit onto an actual display object.

As the above text implies, Layers defer the actual rendering; other stuff doesn't blit to the Layer, it gives it
instructions on how to draw and blit it, which the Layer will execute when it feels the need to. See the docstrings
for Layer itself and its methods for more information on how that actually works.
"""
import weakref

import pygame

class Layer:
    """
    The most basic component of the engine's rendering system. This is the equivalent of a Surface in pygame, if that
    makes it any clearer.

    Basically, other objects give callables to the Layer, and those callables do the actual rendering to the Layer's
    graphical backing. This is a bit weird, but it makes separating drawing from logic a bit easier.

    The renderer API is as such:
    A renderer is just a callable that takes a Pygame surface as its first argument. Renderers take no other
    arguments. They're called when the Layer's.render() is called. The order in which renderers are called is completely
    undefined. The screen will be cleared before any renderer is called.

    This is all kinda experimental, so if it seems weird... that's because it is.
    """

    def __init__(self, fromsurface = None):
        """Fromsurface is the surface that this Layer will render to. If it is not provided, it defaults to the display
        surface returned by pygame.display.get_surface(). This will raise TypeError if you try to do that when there
        is no display surface."""
        self.renderers=set()
        if fromsurface is None:
            self.backing = pygame.display.get_surface() #type: pygame.Surface

            if self.backing is None:
                raise TypeError("Layer was initialized from None, indicating it should use the Pygame display surface, but it does not exist (try calling pygame.display.set_mode() first)")
        else:
            self.backing=fromsurface #type: pygame.Surface

        self.bgcolor=pygame.Color(0,0,0,0)
        self.sublayers=weakref.WeakSet() #type: weakref.WeakSet[Layer]

    def render(self):
        """Execute all the drawing callables and actually blit the stuff to the Pygame Surface that is the backing
        of this Layer. This propagates down to all sublayers as well.

        The order in which renderers are called and the order in which sublayers .render() is called are both
        undefined."""
        self.backing.fill(self.bgcolor)

        for therenderer in self.renderers:
            therenderer(self.backing)

        for thesublayer in self.sublayers:
            thesublayer

    def get_sublayer(self,rect):
        """Return a Layer that represents the part of this Layer that is inside the given rectangle. This is based off
        of Pygame's subsurface functionality; rendering to this layer's surface renders to the parent surface.

        The Renderer keeps weak references to all of its sublayers; when its .render is called, it will also call
        .render on all of its sublayers. This is done in 'top-down' fashion, this layer's renderers are evaluated,
        then all of its sublayers renderers are evaluated (in undefined order)."""

        result = Layer((rect.w,rect.h), fromsurface=self.backing.subsurface(rect))
        result.bgcolor=self.bgcolor

        return result

if __name__ == '__main__':
    pass